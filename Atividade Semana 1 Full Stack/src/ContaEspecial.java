public class ContaEspecial extends Conta{

    private Double limite;

    public void setLimite(Double limite) {
        this.limite = limite;
    }

    public Double getLimite() {
        return limite;
    }

    @Override
    protected boolean temSaldo() {
        return true;
    }

    @Override
    public void abrirConta() {

    }
}
