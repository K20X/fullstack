public class Conta {

    private Integer nrConta;
    private Pessoa cliente;
    private Double saldo;


    public Pessoa getCliente() {
        return cliente;
    }

    public void setCliente(Pessoa cliente) {
        this.cliente = cliente;
    }

    public Integer getNrConta() {
        return nrConta;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void abrirConta(){

    }

    public void sacar(){

    }

    private void debitar(){

    }

    protected boolean temSaldo(){
        return false;
    }

    public void depositar(){

    }

    public void transferir(){

    }

    @Override
    public String toString() {
        return "Conta{" +
                "nrConta=" + nrConta +
                ", cliente=" + cliente +
                ", saldo=" + saldo +
                '}';
    }
}
