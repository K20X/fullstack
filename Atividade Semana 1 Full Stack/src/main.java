import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class main {

    public static void main(String args[]){

        System.out.println("teste");

        String option = "x";

        Integer id = 0;

        List<Conta> contas = new ArrayList<Conta>();

        while(option.equals("0") == false){

            id++;

            option = JOptionPane.showInputDialog(null, "Insira o tipo de Cliente\n0 - Sair\n1- Pessoa Física\n2- Pessoa Jurídica");

            if(option.equals("1")){

                PessoaFisica novoCliente = new PessoaFisica();
                novoCliente.setCpf(JOptionPane.showInputDialog(null, "Insira o CPF"));
                novoCliente.setNome(JOptionPane.showInputDialog(null, "Insira o Nome"));
                novoCliente.setGenero(JOptionPane.showInputDialog(null, "Insira o Gênero\n\nM - Masculino\nF - Feminino"));
                novoCliente.setId(id);
                novoCliente.setEndereco(JOptionPane.showInputDialog(null, "Insira o Endereço"));

                option = JOptionPane.showInputDialog(null, "Insira o tipo de Conta\n0 - Sair\n1- Conta Especial\n2- Conta Poupança");

                if(option.equals("1")){

                    ContaEspecial novaConta = new ContaEspecial();
                    novaConta.setLimite(1000.00);
                    novaConta.setCliente(novoCliente);
                    contas.add(novaConta);

                }else if(option.equals("2")) {

                    ContaPoupanca novaConta = new ContaPoupanca();
                    novaConta.setTxCorrecao(30.0);
                    novaConta.setCliente(novoCliente);
                    contas.add(novaConta);
                }

            }else if(option.equals("2")){

                PessoaJuridica novoCliente = new PessoaJuridica();
                novoCliente.setCnpj(JOptionPane.showInputDialog(null, "Insira o CNPJ"));
                novoCliente.setAtividade(JOptionPane.showInputDialog(null, "Insira a Atividade"));
                novoCliente.setNome(JOptionPane.showInputDialog(null, "Insira o Nome"));
                novoCliente.setId(id);
                novoCliente.setEndereco(JOptionPane.showInputDialog(null, "Insira o Endereço"));

                option = JOptionPane.showInputDialog(null, "Insira o tipo de Conta\n0 - Sair\n1- Conta Especial\n2- Conta Poupança");

                if(option.equals("1")){

                    ContaEspecial novaConta = new ContaEspecial();
                    novaConta.setLimite(1000.00);
                    novaConta.setCliente(novoCliente);
                    contas.add(novaConta);

                }else if(option.equals("2")) {

                    ContaPoupanca novaConta = new ContaPoupanca();
                    novaConta.setTxCorrecao(30.0);
                    novaConta.setCliente(novoCliente);
                    contas.add(novaConta);
                }
            }
        }

        for(int i = 0; i < contas.size(); i++){
            System.out.println(contas.get(i).toString());
        }

    }
}