package br.ufg.inf.fs20211.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_hospedagem")

public class Hospedagem {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_hospedagem")
    Integer idHospedagem;

    @Column(name="id_quarto")
    Integer idQuarto;

    @Column(name="id_hospede")
    Integer idHospede;

    @Column(name="dt_checkin")
    String dtCheckin;

    @Column(name="dt_checkout")
    String dtCheckout;


    public Hospedagem(Integer idHospedagem, Integer idQuarto, Integer idHospede, String dtCheckin, String dtCheckout) {
        this.idHospedagem = idHospedagem;
        this.idQuarto = idQuarto;
        this.idHospede = idHospede;
        this.dtCheckin = dtCheckin;
        this.dtCheckout = dtCheckout;
    }

    public Hospedagem(){
        super();
    }

    public Integer getIdHospedagem() {
        return this.idHospedagem;
    }

    public void setIdHospedagem(Integer idHospedagem) {
        this.idHospedagem = idHospedagem;
    }

    public Integer getIdQuarto() {
        return this.idQuarto;
    }

    public void setIdQuarto(Integer idQuarto) {
        this.idQuarto = idQuarto;
    }

    public Integer getIdHospede() {
        return this.idHospede;
    }

    public void setIdHospede(Integer idHospede) {
        this.idHospede = idHospede;
    }

    public String getDtCheckin() {
        return this.dtCheckin;
    }

    public void setDtCheckin(String dtCheckin) {
        this.dtCheckin = dtCheckin;
    }

    public String getDtCheckout() {
        return this.dtCheckout;
    }

    public void setDtCheckout(String dtCheckout) {
        this.dtCheckout = dtCheckout;
    }

    @Override
    public String toString() {
        return "{" +
            " idHospedagem='" + getIdHospedagem() + "'" +
            ", idQuarto='" + getIdQuarto() + "'" +
            ", idHospede='" + getIdHospede() + "'" +
            ", dtCheckin='" + getDtCheckin() + "'" +
            ", dtCheckout='" + getDtCheckout() + "'" +
            "}";
    }

    
}
