package br.ufg.inf.fs.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_hospedagem")
public class Hospedagem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_hospedagem")
	private Integer idHospedagem;
	
	@OneToOne
	@JoinColumn(name="hotel_id")
	private Hotel hotel;

	
	@Column(name="quantidade_leito")
	private Integer qtdLeito;
	
	@Column(name="numero_hospedagem")
	private Integer nrHospedagem;
	
	@Column(name="preco_diaria")
	private Double prDiaria;

	public Hospedagem() {
		super();
	}

	public Hospedagem(Integer idHospedagem, Hotel hotel, Integer qtdLeito, Integer nrHospedagem,
			Double prDiaria) {
		super();
		this.idHospedagem = idHospedagem;
		this.hotel = hotel;
		this.qtdLeito = qtdLeito;
		this.nrHospedagem = nrHospedagem;
		this.prDiaria = prDiaria;
	}

	public Integer getIdHospedagem() {
		return idHospedagem;
	}

	public void setIdHospedagem(Integer idHospedagem) {
		this.idHospedagem = idHospedagem;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Integer getQtdLeito() {
		return qtdLeito;
	}

	public void setQtdLeito(Integer qtdLeito) {
		this.qtdLeito = qtdLeito;
	}

	public Integer getNrHospedagem() {
		return nrHospedagem;
	}

	public void setNrHospedagem(Integer nrHospedagem) {
		this.nrHospedagem = nrHospedagem;
	}

	public Double getPrDiaria() {
		return prDiaria;
	}

	public void setPrDiaria(Double prDiaria) {
		this.prDiaria = prDiaria;
	}

	@Override
	public String toString() {
		return "Hospedagem [idHospedagem=" + idHospedagem + ", hotel=" + hotel
				+ ", qtdLeito=" + qtdLeito + ", nrHospedagem=" + nrHospedagem + ", prDiaria=" + prDiaria + "]";
	}

}
