package br.ufg.inf.fs.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufg.inf.fs.entities.Hospedagem;

public interface HospedagemRepository extends JpaRepository<Hospedagem, Integer>{

	@Query("SELECT h FROM Hospedagem h WHERE h.nmHospedagem LIKE %:str%")
	public List<Hospedagem> findByNmHospedagem(@Param("str") String str);
	
	
	@Query("SELECT h FROM Hospedagem h WHERE h.qtdEstrelas = :qtd")
	public List<Hospedagem> findByQtdEstelas(@Param("qtd") Integer qtd);
}
