package br.ufg.inf.fs.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_hospede")
public class Hospede implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_hospede")
	private Integer idHospede;
	
	@OneToOne
	@JoinColumn(name="hotel_id")
	private Hotel hotel;
	
	@Column(name="quantidade_leito")
	private Integer qtdLeito;
	
	@Column(name="numero_hospede")
	private Integer nrHospede;
	
	@Column(name="preco_diaria")
	private Double prDiaria;

	public Hospede() {
		super();
	}

	public Hospede(Integer idHospede, Hotel hotel, Integer qtdLeito, Integer nrHospede,
			Double prDiaria) {
		super();
		this.idHospede = idHospede;
		this.hotel = hotel;
		this.qtdLeito = qtdLeito;
		this.nrHospede = nrHospede;
		this.prDiaria = prDiaria;
	}

	public Integer getIdHospede() {
		return idHospede;
	}

	public void setIdHospede(Integer idHospede) {
		this.idHospede = idHospede;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Integer getQtdLeito() {
		return qtdLeito;
	}

	public void setQtdLeito(Integer qtdLeito) {
		this.qtdLeito = qtdLeito;
	}

	public Integer getNrHospede() {
		return nrHospede;
	}

	public void setNrHospede(Integer nrHospede) {
		this.nrHospede = nrHospede;
	}

	public Double getPrDiaria() {
		return prDiaria;
	}

	public void setPrDiaria(Double prDiaria) {
		this.prDiaria = prDiaria;
	}

	@Override
	public String toString() {
		return "Hospede [idHospede=" + idHospede + ", hotel=" + hotel
				+ ", qtdLeito=" + qtdLeito + ", nrHospede=" + nrHospede + ", prDiaria=" + prDiaria + "]";
	}

}
