package br.ufg.inf.fs.entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_hospede")

public class Hospede {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_hospede")
	private Integer idHospede;

    @Column(name="nm_hospede")
	private String nmHospede;

    @Column(name="dt_nascimento")
	private String dtNascimento;

    @Column(name="cpf")
	private String cpf;

    public Hospede(){
        super();
    }

    public Hospede(Integer idHospede, String nmHospede, String dtNascimento, String cpf){
        super();
        this.idHospede = idHospede;
        this.nmHospede = nmHospede;
        this.dtNascimento = dtNascimento;
        this.cpf = cpf;
    }

    public Integer getIdHospede()
    {
        return this.idHospede;
    }

    public String getNmHospede()
    {
        return this.nmHospede;
    }

    public String getDtNascimento(){
        return this.dtNascimento;
    }

    public String getCpf(){
        return this.cpf;
    }

    public void setIdHospede(Integer idHospede){
        this.idHospede = idHospede;
    }


    public void setNmHospede(String nmHospede)
    {
       this.nmHospede = nmHospede;
    }

    public void setDtNascimento(String dtNascimento){
        this.dtNascimento = dtNascimento;
    }

    public void setCpf(String cpf){
        this.cpf = cpf;
    }

    @Override
    public String toString() {
        return "{" +
            " idHospede='" + getIdHospede() + "'" +
            ", nmHospede='" + getNmHospede() + "'" +
            ", dtNascimento='" + getDtNascimento() + "'" +
            ", cpf='" + getCpf() + "'" +
            "}\n\n";
    }


}
