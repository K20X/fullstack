package br.ufg.inf.fs.ctrl;

import java.io.Console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufg.inf.fs.entities.Hospede;

@RestController
@RequestMapping(value="fullstack")
public class fullStack {

    @GetMapping
	public String fullstack() {
		return "Até aqui, tudo funcionou";
	}
}
