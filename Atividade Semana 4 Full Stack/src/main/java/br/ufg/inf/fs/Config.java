package br.ufg.inf.fs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import br.ufg.inf.fs.entities.Hospedagem;
import br.ufg.inf.fs.entities.Hospede;
import br.ufg.inf.fs.enums.CategoriaQuarto;
import br.ufg.inf.fs.repositories.QuartoRepository;
import br.ufg.inf.fs.repositories.HospedeRepository;
import br.ufg.inf.fs.repositories.HospedagemRepository;

@Configuration
@Profile("root")
public class Config  implements CommandLineRunner{

	@Autowired
	private HospedeRepository hospedeRepository;

	@Autowired
	private HospedagemRepository hospedagemRepository;
	
	@Override
	public void run(String... args) throws Exception {

		System.out.println("\n\n\n-----VAMOS COMEÇAR ESTE PROGRAMA----\n\n\n");

		// TODO Auto-generated method stub
	
		/*
		 * INSERIR NO MEU BANCO DE DADOS INFORMAÇÕES INICIAIS...
		 * */
		
		/*Hotel h1 = new Hotel(null, "Calderão Furado", "Beco Diagonal", 3);
		Hotel h2 = new Hotel(null, "Bates Hotel", "White Pine Bay", 2);
		Hotel h3 = new Hotel(null, "Hotel Overlook", "Colorado", 4);
		Hotel h4 = new Hotel(null, "Continental Hotel", "Ney York City", 5);
		
		h1 = hoteRepository.save(h1);
		h2 = hoteRepository.save(h2);
		h3 = hoteRepository.save(h3);
		h4 = hoteRepository.save(h4);
		
		Quarto q1 = quartoRepository.save(new Quarto(null, h1, CategoriaQuarto.APARTAMENTO, 4, 1010, 200.0));
		Quarto q2 = quartoRepository.save(new Quarto(null, h2, CategoriaQuarto.SIMPLES, 1, 7, 100.0));
		Quarto q3 = quartoRepository.save(new Quarto(null, h3, CategoriaQuarto.PADRAO, 2, 306, 150.0));
		Quarto q4 = quartoRepository.save(new Quarto(null, h4, CategoriaQuarto.LUXO, 3, 1313, 500.0));*/

		/*Hospede hospede1 = new Hospede(null, "MAURICIO RODRIGUES LIMA", "2000-03-01", "706");
		Hospede hospede2 = new Hospede(null, "TESTE DE NOME", "2000-06-27", "4045");

		hospede1 = hospedeRepository.save(hospede1);
		hospede2 = hospedeRepository.save(hospede2);

		Hospedagem hospedagem1 = new Hospedagem(null, 1, 1, "2021-08-24", "2021-09-24");
		Hospedagem hospedagem2 = new Hospedagem(null, 2, 2, "2021-08-25", "2021-08-28");

		hospedagem1 = hospedagemRepository.save(hospedagem1);
		hospedagem2 = hospedagemRepository.save(hospedagem2);*/


		Hospede hospede1 = new Hospede(null, "DISCIPLINA FULL STACK", "2000-04-02", "123");
		Hospede hospede2 = new Hospede(null, "UNIVERSIDADE FEDERAL DE GOIÁS", "2000-06-27", "432");

		hospede1 = hospedeRepository.save(hospede1);
		hospede2 = hospedeRepository.save(hospede2);

		Hospedagem hospedagem1 = new Hospedagem(null, 3, 3, "2021-08-24", "2021-09-24");
		Hospedagem hospedagem2 = new Hospedagem(null, 3, 3, "2021-08-25", "2021-08-28");

		hospedagem1 = hospedagemRepository.save(hospedagem1);
		hospedagem2 = hospedagemRepository.save(hospedagem2);

	}

}
